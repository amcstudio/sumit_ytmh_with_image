"use strict";
~(function () {
  var
    bgExit = document.getElementById("bgExit"),
    ad = document.getElementById("mainContent"),
    close = document.getElementById("closeButton"),
    tl = new TimelineMax({}); 
    bgExit.addEventListener('click', bgExitHandler, false);

  window.init = function () {

    addListeners();
    tl.set("#mainContent", {force3D: true });
   
    tl.to("#lightBlueCopyOne", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to("#copyOne", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to(["#copyOne","#lightBlueCopyOne"], 0.5, { opacity: 0, ease: Power1.easeOut }, "+=2.4");

    tl.to("#parisBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#londonBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to("#lightBlueCopyTwo", 0.5, { x: 0, ease: Power1.easeOut },"-=0.5");
    tl.to("#copyTwo", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to(["#copyTwo", "#lightBlueCopyTwo"],0.5, {opacity:0, ease:Power1.easeOut},"+=2.7");

    tl.to("#frankfurtBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#parisBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to("#lightBlueCopyThree", 0.5, { x: 0, ease: Power1.easeOut }, "-=0.5");
    tl.to("#copyThree", 0.5, { x:0, ease:Power1.easeOut});
    tl.to(["#copyThree",".lightBlueCopy"] ,0.5, {opacity:0, ease:Power1.easeOut},"+=2.4");

    tl.to("#endFrameBg", 0.5, { opacity:1, rotation: 0.01, ease: Sine.easeOut },"-=0.5");
    tl.to("#frankfurtBg", 0.5, { opacity:0, rotation: 0.01, ease: Sine.easeInOut },"-=0.5");
    tl.to(".logo", 0.5, { scale:.82, x: -15, y:-16, rotation:0.01, ease: Power1.easeOut }, "-=0.5");
    tl.to("#copyFour", 0.5, { x: 0, ease: Power1.easeOut });
    tl.to("#ctaContainer",0.5, { opacity:1, ease:Power1.easeOut},"+=.5");
    tl.to("#ctaContainer", 0.5, {scale: 1.1, yoyo: true, repeat: 1, force3D: false, rotation: 0.01, ease: Power1.easeInOut },"+=.5");
  };


  function addListeners() {
     bgExit.addEventListener('click', exitClickHandler);
     close.addEventListener('click', closeAd);
  }


  function exitClickHandler() {
    tl.pause();

    var tll = new TimelineMax({});
    tll.set(["#endFrameBg",'#ctaContainer'] , {opacity:1});
    tll.set("#copyFour" , {x:0});
    tll.set(".logo" , {scale:.82, x: -15, y:-16});
    tll.set(["#londonBg","#parisBg","#frankfurtBg","#lightBlueCopyOne","#lightBlueCopyTwo","#lightBlueCopyThree",
    "#copyOne","#copyTwo","#copyThree"], {opacity:0});

    Enabler.exit('BackgroundExit');
  
  }
  function closeAd() {
    ad.style.display = "none";
  
  }

	//clicktag
	function bgExitHandler() {
    Enabler.exit('Background Exit');
    }
  
})();
